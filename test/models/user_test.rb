require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(name: "joao1", email: "joaoao@gmail.com", password: "12345678", password_confirmation: "12345678")
  end

  test "should be valid" do
      assert @user.valid?
  end

  test "name should be present" do
    @user.name = ""
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = "      "
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = "="*2313
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "="*2313
    assert_not @user.valid?
  end

end